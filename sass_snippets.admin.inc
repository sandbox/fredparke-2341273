<?php
/**
 * @file
 * Admin settings form.
 */

/**
 * Implements hook_admin().
 *
 * Create admin settings for for Sass snippets module.
 */
function sass_snippets_admin() {
  $form = array();

  // Sass snippets general settings.
  if (function_exists('libraries_get_path') && !file_exists(libraries_get_path('scssphp') . '/scss.inc.php')) {
    $form['sass_snippets_info'] = array(
      '#markup' => t('<h2>This module requires the scssphp library</h2>
        <p>The !lib library should be placed in a folder called scssphp inside your libraries folder. Eg: <code>sites/all/libraries/scssphp/scss.inc.php</code></p>', array(
          '!lib' => l(t('scsphp library'), 'http://leafo.net/scssphp/'),
        )),
    );
  }
  $form['sass_snippets_import_paths'] = array(
    '#type' => 'textarea',
    '#title' => t("Import paths"),
    '#default_value' => variable_get('sass_snippets_import_paths', ''),
    '#description' => t("Import paths to be included. One per line. <b>Example:</b><br/><i>sites/all/libraries/compass/frameworks/compass/stylesheets</i>"),
  );
  $form['sass_snippets_pre_import'] = array(
    '#type' => 'textarea',
    '#title' => t("Pre import"),
    '#default_value' => variable_get('sass_snippets_pre_import', ''),
    '#description' => t('Code written here will be automatically and silently imported every time sass is compiled. <b>Example:</b><br/><i>$legacy-support-for-ie6: false;<br/>$legacy-support-for-ie7: false;</i>'),
  );
  $form['sass_snippets_switcher'] = array(
    '#type' => 'checkbox',
    '#title' => t("Sass snippets switcher"),
    '#default_value' => variable_get('sass_snippets_switcher', 1),
    '#description' => t("Use the sass snippets theme for code switching."),
  );
  $form['sass_snippets_output_format'] = array(
    '#type' => 'select',
    '#title' => t("Output format"),
    '#options' => array(
      'scss_formatter' => t('scss_formatter'),
      'scss_formatter_nested' => t('scss_formatter_nested'),
      'scss_formatter_compressed' => t('scss_formatter_compressed'),
    ),
    '#default_value' => variable_get('sass_snippets_output_format', 'scss_formatter_nested'),
    '#description' => t("Customize the formatting of the output CSS by changing the formatter."),
  );
  return system_settings_form($form);
}
