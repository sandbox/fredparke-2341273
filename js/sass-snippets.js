/**
 * @file
 * Sass snippet switching functionality.
 */

(function ($) {

  Drupal.behaviors.sassSnippetSwitcher = {
    attach: function (context) {
      $('body').once('sass-snippets', function () {
        // Create entry form close.
        var switcher = $('<a>').attr('href', '').addClass('sass-snippet-switcher').html('<span class="scss">SCSS</span><span class="css">CSS</span>');
        switcher.prependTo($('.sass-snippet-wrapper'));

        $('.sass-snippet-switcher').click(function(e) {
          e.preventDefault();
          $(this).parent().toggleClass('css-on');
          $(this).parent().toggleClass('scss-on');
        });
      });
    }
  }

})(jQuery);
