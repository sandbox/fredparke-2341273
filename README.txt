
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Fred Parke <mail@fredparke.co.uk>

The Sass Snippets module provides a text filter which uses the scssphp compiler
for SCSS to generate css versions of SCSS code snippets.

The older sass "indented syntax" is not supported.

This module requires the scssphp library. The scssphp library should be placed
in a folder called scssphp inside your libraries folder. Eg:

sites/all/libraries/scssphp/scss.inc.php


INSTALLATION
------------

1. Install the module in the usual way (http://drupal.org/node/895232)
2. Ensure the scssphp library is present (see above)
3. Any additional sass libraries (such as Compass) that are present can be added
to the 'Import paths' in the module configuration page:
admin/config/content/sass-snippets. Tested and partially working with
compass 0.12.2. Folders containing .scss files can also be imported.
4. Enable the Sass Snippets filter on one of your text formats at:
/admin/config/content/formats
4. Insert SCSS into a <pre> tag with the class sass-snippet. Example:
